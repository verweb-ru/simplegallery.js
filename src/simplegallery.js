(function () {
    this.SimpleGallery = function () {
        let defaults = {
            selector: null,
            params: {
                direction: 'left',
                speed: 1000,
            }
        }

        if (arguments[0] && typeof arguments[0] === 'string') {
            defaults = { ...defaults, selector: arguments[0] }
        }

        if (arguments[1] && typeof arguments[1] === 'object') {
            this.options = {
                ...defaults,
                params: {
                    ...defaults.params,
                    ...arguments[1]
                }
            }
        } else {
            this.options = {
                ...defaults,
                params: {
                    ...defaults.params
                }
            }
        }

        this.init()
    }

    function setGalleryHtml (mainImg, images) {
        let html = `<div class="simplegallery__images">`
        images.forEach(img => { html += img.outerHTML })
        html += `</div><div class="simplegallery__main-image">${mainImg.outerHTML}</div>`
        return html
    }

    function listenImageClick (parent) {
        document.querySelectorAll(`${parent} .simplegallery__images img`).forEach(image => {
            image.addEventListener('click', () => {
                document.querySelector(`${parent} .simplegallery__main-image img`).setAttribute('src', image.getAttribute('src'))
            })
        })
    }

    SimpleGallery.prototype.init = function () {
        const gallery = document.querySelector(this.options.selector)
        const images = Array.prototype.slice.call(gallery.childNodes).filter(img => img.tagName === 'IMG')
        const mainImage = images[0]
        gallery.innerHTML = setGalleryHtml(mainImage, images)
        listenImageClick(this.options.selector)
    }
}())
